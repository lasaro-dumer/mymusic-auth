﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace MyMusic.Auth
{
    public class InMemoryConfiguration
    {
        public static IEnumerable<ApiResource> ApiResources()
        {
            return new[] {
                new ApiResource("mymusic", "My Music API")
                {
                    UserClaims = new List<string>(){ JwtClaimTypes.Role }
                }
            };
        }

        public static IEnumerable<Client> Clients()
        {
            return new[] {
                new Client
                {
                    ClientId = "mymusic",
                    ClientSecrets = new [] { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = new [] { "mymusic" }
                }
            };
        }

        public static IEnumerable<TestUser> Users()
        {
            return new[] {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "user@mymusic.com",
                    Password = "password",
                    Claims = new List<Claim>(){
                        new Claim(JwtClaimTypes.Role, "api_user_read")
                    }
                }
            };
        }
    }
}
